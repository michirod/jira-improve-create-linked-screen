# Improve Create Linked Issue screen for Jira Core Cloud

This extension will show all the hidden fields in the Create Linked Issue screen.

It works for Jira Core Cloud instances.

It may work for Jira Server, I have not tested it.

## Install

### Firefox

Install from [firefox addons repo](https://addons.mozilla.org/en-US/firefox/addon/jira-create-linked-issue/)

### Chromium-based browsers (Chrome, Brave, Opera, Edge and others)

I'm not going to create a google account and pay 5$ just to distribute a stupid extension.

1. Download the extension (clone repo or download source code from gitlab and extract it).
2. Go to [chrome://extensions](chrome://extensions) and enable Developer mode.
3. Click "Load Unpacked" and select the extension code folder.
4. You can disable Developer Mode now.

Or:

1. Get Firefox.
