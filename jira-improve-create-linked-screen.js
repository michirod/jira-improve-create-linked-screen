// Check if page is JIRA application
try {
	var jira = document.querySelector('meta[name="application-name"]').content == "JIRA";
} catch (e) {
	jira = false;
}
// only go ahead if page is JIRA application
if (jira) {
	function forceUIMod() {
		// this will force all hidden fields to appear on "Create as Linked" screen
		create_linked_screen = document.getElementById("create-linked-issue-dialog");
		if (create_linked_screen) {
			//console.log("show hidden fields");
			fields = create_linked_screen.getElementsByClassName("qf-field");
			for (let item of fields){
				if (! item.classList.contains("qf-field-active")) {
					item.style.display = "block";
					//item.classList.add("qf-field-active");
				}
			}
		}
	}

	function observerCallback(mutationList, observer){
		//console.log("observer triggered");
		setTimeout(forceUIMod, 500);
	}

	// Options for the observer (which mutations to observe)
	const cfg = { attributes: false, childList: true, subtree: false };
	// Create an observer instance linked to the callback function
	const obs = new MutationObserver(observerCallback);

	// Start observing the target node for configured mutations
	obs.observe(document.getRootNode().body, cfg);

	//Run the function periodically (not good)
	//setInterval(forceUIMod, 2000);
}
